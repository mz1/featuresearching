module Util
  ( groupCount
  , groupPct
  , toTuple
  , isTab
  , isStrand
  , isUnderScore
  , text2Int
  )
where

{-
Project name: Util library
Min Zhang
Date: Oct 9, 2015
Version: v.0.2.0
README: Add type signature for all the functions
-}


import qualified Data.List as L
import qualified Data.Text as T
import Data.Ord (comparing)
import qualified Data.Attoparsec.Text as AP

-- utility functions
groupCount :: Ord a => [a] -> [(a, Int)]
groupCount x = zip (map head groupedOnes) (map length groupedOnes)
  where groupedOnes = groupDes x
       
groupPct :: (Ord a, Fractional b) => [a] -> [(a, b)] 
groupPct l = zip (map head groupedOnes) (map pct groupedOnes) 
  where groupedOnes = groupDes l
        totalLength = L.genericLength l
        pct x = L.genericLength x / totalLength

groupDes :: Ord a => [a] -> [[a]]
groupDes x = L.reverse . L.sortBy (comparing length) . L.group . L.reverse . L.sort $ x

toTuple obj f = (f obj, obj) 

-- Parsing Utils
isTab = AP.isHorizontalSpace
isStrand c = c == '+' || c == '-'
isUnderScore c = c == '_'

text2Int :: T.Text -> Int
text2Int = read . T.unpack
