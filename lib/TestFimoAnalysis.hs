{-

  Min Zhang
  July 25, 2016

  ATAC-Seq analysis
  
  Fimo parsing and analyze the combination of motifs.

-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

import qualified Data.Text as T
import qualified Data.Text.IO as TextIO
import Control.Applicative
import qualified Data.List as L
import qualified Data.Set as Set
import qualified Data.HashMap.Lazy as M
import qualified Data.Maybe as Maybe
import Control.Lens
import Control.Monad (fmap)
import Data.Ord (comparing)
import Data.Function (on)
import qualified Safe as S
import qualified Data.Attoparsec.Text as AP
import qualified Data.Char as C
import System.IO

import Fimo
import NarrowPeaks
import Util
import SuperEnhancers

sample = "/Users/minzhang/Documents/data/P60_heart_dropseq/AtacSeqAnalysis/data/fimo_out/fimo.annotated.txt"

sePath = "/Users/minzhang/Documents/data/P60_heart_dropseq/AtacSeqAnalysis/data/output_mergedSE.bed"


-- | test parsing annotated Fimo file
--main = do
--  h <- openFile sample ReadMode
--  str <- TextIO.hGetLine h
--  hClose h
--  return $ AP.feed (AP.parse fimoParser str) T.empty

-- | load the whole file
--main = do
--  fimo <- Maybe.fromJust . readFimo <$> TextIO.readFile sample
--  print $ length fimo

-- | count what's the combination of different tissues
--main = do
--  fimo <- readFimo <$> TextIO.readFile sample
--  let npMap = mergeNP $ map toTuple $ Maybe.fromJust fimo
--  let res = groupCount $ M.elems $ M.map Set.fromList $ getSource npMap
--  print res

-- | count motifs specfically in neo_cm_np
--main = do
--  fimo <- readFimo <$> TextIO.readFile sample
--  let npMap = mergeNP $ map toTuple $ Maybe.fromJust fimo
--  let res = groupCount $ concat $ M.elems $ M.map (map motif . tf) $ filterBySource [Tissue "neo_cm_np"] npMap
--  print res

--main = do
--  fimo <- readFimo <$> TextIO.readFile sample
--  let npMap = mergeNP $ map toTuple $ Maybe.fromJust fimo
--  let res = groupCount $ concat $ M.elems $ M.map (map motif . tf) $ filterBySource [Tissue "adult_tcfmut"] npMap
--  print res

main = do
  h <- openFile sePath ReadMode
  str <- TextIO.hGetLine h
  hClose h
  return $ AP.feed (AP.parse seParser str) T.empty

