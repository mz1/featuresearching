#!/bin/ruby

# Min Zhang
# June 23, 2016

# Separate list of transcripts into individual files, and sorted by tx start site

# usage: 
# ruby indexGeneList <input gene table> <output directory>

# the input file format is UCSC table genePredExt

def main
  inputpath = ARGV[0]
  outputdir = ARGV[1]
#  inputpath = "ensGene.txt"
#  outputdir = "mm10"
  
  # mkdir
  if File.directory?(outputdir)
  then 
      puts "#{outputdir} exists. Remove all the files in the folder."
      %x{rm -rf #{outputdir + "/*"} }
  else 
      %x{mkdir #{outputdir}}
  end

  # group by chr
  # if reverse then use tx end site 
  chrGroups = File.read(inputpath).split("\n").map{|x| x.split("\t")}.map{|x| if x[3] == "+" then x += [x[4]] else x += [x[5]] end}.group_by{|x| x[2]}.to_a
  chrGroups.collect do
    |chr| 
          txt = chr[1].sort_by{|x| x.last.to_i}.map{ |x| x.join("\t")}.join("\n")
          File.write(outputdir + "/" + chr[0] + ".gene", txt) 
          nil
  end
end

main

