{-

  Min Zhang
  July 25, 2016

  ATAC-Seq analysis
  
  Fimo parsing and analyze the combination of motifs.

-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module SuperEnhancers
  ( readSE
  , seToHash
  , SuperEnhancer (..))
where

import qualified Data.Text as T
import Control.Applicative
import qualified Data.List as L
import qualified Data.Set as Set
import qualified Data.HashMap.Lazy as M
import qualified Data.Maybe as Maybe
import Control.Lens
import Control.Monad (fmap)
import Data.Ord (comparing)
import Data.Function (on)
import qualified Safe as S
import qualified Data.Attoparsec.Text as AP
import qualified Data.Char as C
import qualified Data.Scientific as Sci

import Util

-- | parse SuperEnhancers in Bed format
data SuperEnhancer = SE
    { chr_se    :: T.Text
    , start_se  :: Int
    , end_se    :: Int
    , source    :: [T.Text]
    }
    | NullSE deriving (Show, Read, Eq)

-- | Note: last line is not parsed

seParser = do
  chr <- AP.takeTill isTab
  AP.skip isTab
  start <- text2Int <$> AP.takeWhile C.isDigit
  AP.skip isTab
  end <- text2Int <$> AP.takeWhile C.isDigit
  AP.skip isTab
  source <- T.splitOn ";" <$> AP.takeTill isTab
  _ <- AP.takeTill AP.isEndOfLine
  AP.skip AP.isEndOfLine
  return (SE chr start end source)
--  return (start, end, source)

parseManySE = AP.many1' seParser
readSE txt = AP.maybeResult $ AP.feed (AP.parse parseManySE txt) T.empty

-- | to make hashMap 
toTupleSe se = (chr_se se, [se])

-- | make hashMap of SE by chr and sort in each chr by start
seToHash = M.map (L.sortBy (comparing start_se)) . M.fromListWith (++) . map toTupleSe

