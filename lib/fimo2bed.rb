# Min Zhang
# Martin lab

# May 19, 2016

# turn fimo output to bed file

# June 24, 2016
# edit for findFeatures use, generate bed file?

def readFimo(inputpath)
    input = File.read(inputpath).split("\n").select{|x| x[0] != "#"}.map{|x| x.split("\t")}
end

def fimo2Bed(fimo, chr, add)
    def addInterval(fimoInterval, additional)
        (fimoInterval.to_i + additional).to_s
    end
    start_bed = addInterval(fimo[2], add)
    end_bed = addInterval(fimo[3], add)
    strand = fimo[4]
    score = fimo[5]
    name = fimo[8]
    thickStart = start_bed
    thickEnd = end_bed
    itemRgb = if strand == "+"; "204,0,0" else "0,76,153" end
    pvalue = fimo[6].to_f
    bed = [chr, start_bed, end_bed, name, score, strand, thickStart, thickEnd, itemRgb, pvalue]
end
 
  
def main
    inputfile = ARGV[0]
    input = readFimo(inputfile).map{|x| fimo2Bed(x, "chr4", 1)} 
    e6 = input.select{|x| x[-1] <= 1e-6}.map{|x| x[0..-2].join("\t")}.join("\n")
    e5 = input.select{|x| x[-1] <= 1e-5 && x[-1] > 1e-6}.map{|x| x[0..-2].join("\t")}.join("\n")
    e4 = input.select{|x| x[-1] <= 1e-4 && x[-1] > 1e-5}.map{|x| x[0..-2].join("\t")}.join("\n")
    File.write("./output_e6.bed", e6)
    File.write("./output_e5.bed", e5)
    File.write("./output_e4.bed", e4)
end

main
