#!/bin/ruby

## Min Zhang
## June 23, 2016

## given a location in the format of chr:start-end return a list of features.

class Loc
  attr_accessor :chr, :start, :end_, :loc, :label

  def validChr?
    self.chr[0..2] == "chr" 
  end

  def validInterval?
    self.start < self.end_  
  end

  def isValid?
    self.validChr? && self.validInterval?
  end

  def to_s
    [self.chr, self.start, self.end_].join("_")
  end
 
  def to_bed
    bed = Bed.new
    bed.chr = self.chr
    bed.chrStart = self.start
    bed.chrEnd = self.end_
    bed.name = [self.chr, self.start, self.end_, self.label.to_s].join("_")
    bed.score = "0"
    bed.strand = "+"
    bed
  end

end

class Mode
  attr_accessor :mode, :uplimit, :downlimit
end


class String
   def to_loc(sep = ":")
      sArr = self.gsub(",", "").gsub("\t", ":").gsub(" ", ":").gsub("-", ":").split(sep)
      if sArr.size != 3 || sArr[0][0..2] != "chr" || sArr[1] =~/[0-9]/.nil? || sArr[2] =~/[0-9]/.nil?
      then return "Error: bad format for interval"
      else loc = Loc.new
           loc.chr = sArr[0]
           loc.start = sArr[1].to_i
           loc.end_ = sArr[2].to_i
           loc.loc = loc.start
           loc.label = nil
           return loc
      end
   end

   def to_mode
      sArr = self.gsub(" ","").split(",")
      mode = Mode.new
      mode.mode = sArr[0]
      mode.uplimit = sArr[1].to_i
      mode.downlimit = sArr[2].to_i
      return mode
   end
end

class Array
    def to_gene
        unless self.size == 17
          raise ArgumentError,
             "gene needs have exact 17 fields."
        end
        gene = Gene.new
        gene.bin = self[0]
        gene.ensTxName = self[1]
        gene.chr = self[2]
        gene.strand = self[3]
        gene.tx_start = self[4]
        gene.tx_end = self[5]
        gene.cds_start = self[6]
        gene.cds_end = self[7]
        gene.num_exon = self[8]
        gene.exon_start = self[9]
        gene.exon_end = self[10]
        gene.uniq_id = self[11]
        gene.altName = self[12]
        gene.cdsStartStat = self[13]
        gene.cdsEndStat = self[14]
        gene.exonFrame = self[15]
        gene.loc = self[16].to_i
        gene.label = nil
        return gene
    end

    def to_expr
        unless self.size == 18
          raise ArgumentError,
             "Expression data needs have exact 17 fields."
        end
        expr = Expr.new
        expr.tx_name = self[0]
        expr.loc = self[1].to_i
        expr.length = self[2]
        expr.symbol = self[3]
        expr.v90_status = self[5] # status first, bc it needs to check high data
        expr.v90_fpkm = if expr.v90_status == "HIDATA" then "2000" else self[4] end
        expr.v10_status = self[7]
        expr.v10_fpkm = if expr.v10_status == "HIDATA" then "2000" else self[6] end
        expr.v1_status = self[9]
        expr.v1_fpkm = if expr.v1_status == "HIDATA" then "2000" else self[8] end
        expr.cf28_status = self[11]
        expr.cf28_fpkm = if expr.cf28_status == "HIDATA" then "2000" else self[10] end
        expr.cf1_status = self[13]
        expr.cf1_fpkm = if expr.cf1_status == "HIDATA" then "2000" else self[12] end
        expr.cm30_status = self[15]
        expr.cm30_fpkm = if expr.cm30_status == "HIDATA" then "2000" else self[14] end
        expr.cm1_status = self[17]
        expr.cm1_fpkm = if expr.cm1_status = "HIDATA" then "2000" else self[16] end
        return expr 
    end

    def to_ctcf
        ctcf = Ctcf.new
        name = self[1].split("_")
        ctcf.motif_name = self[0]
        ctcf.peak_name = self[1]
        ctcf.peak_score = name[3]
        ctcf.chr = name[0]
        ctcf.start = name[1]
        ctcf.end_ = name[2]
        ctcf.motif_start = self[2]
        ctcf.motif_end = self[3]
        ctcf.motif_strand = self[4]
        ctcf.motif_score = self[5]
        ctcf.motif_pvalue = self[6]
        ctcf.motif_qvalue = self[7]
        ctcf.seq = self[8]
        ctcf.loc = ctcf.start.to_i
        ctcf.label = 0
        return ctcf
    end
end 

class Gene
   # uniq_id is meanless here
   attr_accessor :bin, :ensTxName, :chr, :strand, :tx_start, :tx_end, :cds_start, :cds_end, :num_exon, :exon_start, :exon_end, :uniq_id, :altName, :cdsStartStat, :cdsEndStat, :exonFrame, :loc, :symbol, :ctcf, :expr, :rank, :label
   def initialize
     @symbol = ""
     @rank = 0
     @ctcf = 0
     @expr = nil
     super
   end

   def to_bed
      bed = Bed.new
      bed.chr = self.chr
      bed.chrStart = self.loc - 250
      bed.chrEnd = self.loc + 250
      bed.name = self.symbol + "_" + self.label.to_s
      bed.score = if self.expr.nil? 
                  then "0"
                  else self.expr.v10_fpkm
                  end
      bed.strand = self.strand
      bed
   end
end

class Expr
    attr_accessor :tx_name, :loc, :length, :symbol, :v90_fpkm, :v90_status, :v10_fpkm, :v10_status, :v1_fpkm, :v1_status, :cf28_fpkm, :cf28_status, :cf1_fpkm, :cf1_status, :cm30_fpkm, :cm30_status, :cm1_fpkm, :cm1_status
end

class Ctcf
  attr_accessor :motif_name, :peak_name, :peak_score, :chr, :start, :end_, :motif_start, :motif_end, :motif_strand, :motif_score, :motif_pvalue, :motif_qvalue, :seq, :loc, :label
 
  def to_bed
    bed = Bed.new
    bed.chr = self.chr
    bed.chrStart = self.start
    bed.chrEnd = self.end_
    bed.name = self.peak_name + "_" + self.label.to_s
    bed.score = self.peak_score
    bed.strand = self.motif_strand
    bed
  end
end 

class Bed
  attr_accessor :chr, :chrStart, :chrEnd, :name, :score, :strand
  def to_s
    [self.chr, self.chrStart, self.chrEnd, self.name, self.score, self.strand].join("\t")
  end
end

class Domain
  attr_accessor :previous, :current, :next
end


 
# main func   

# return an index int, where the closest feature is in the index file (.gene)
# interval should be a small region, otherwise it will make loc_med inaccurate
def findGenes(interval, mode, indexDir = "mm9")
   loc = interval.to_loc
   loc_med = (loc.start + loc.end_).div(2)

   # should expand more features
   features = File.read(indexDir + "/" + loc.chr + ".gene").split("\n").map{|x| x.split("\t")}.sort_by{|x| x.last.to_i} 

   # list of loction of features. reverse genes are using bigger int (end_ position)
   # only need last column to be used for sort location and identify index
   refLoc = features.map{|x| x.last.to_i}

   # closest two indices to the region of interest(interval)
   locIndex1 = refLoc.each_index.select{|i| refLoc[i] <= loc_med}.last
   locIndex2 = refLoc.each_index.select{|i| refLoc[i] >= loc_med}.first 

   # locIndex 1 and 2 should be next to each other, or the same; index start from 0 
   # include original middle point (absolute location) of the input region
   ind = [loc_med, locIndex1, locIndex2]
   genes = searchFeature(ind, features, mode).map{|x| x.to_gene}
end 

# feature list needs to be sorted by last column, which is int already
# mode is a string of three fields, separated by comma
def searchFeature(ind, featureList, mode) 
   m = mode.to_mode
   refLoc = featureList.map{|x| x.last.to_i} # get inex
   loc_med = ind[0]
   case m.mode
   when "count"
       return featureList[ind[1] - m.uplimit .. ind[2] + m.downlimit]
   when "distance"
       indexUpBound = refLoc.each_index.select{|i| refLoc[i] <= (loc_med - m.uplimit) }  # getIndex
       indexLowerBound = refLoc.each_index.select{|i| refLoc[i] >= (loc_med + m.downlimit) }
       if indexLowerBound.size != 0 
       then indexLowerBound = indexLowerBound.first
       else indexLowerBound = refLoc.size - 1
       end
       
       if indexUpBound.size != 0 
       then indexUpBound = indexUpBound.last
       else indexUpBound= 0
       end

       return featureList[indexUpBound .. indexLowerBound]
   end
end

# getBoundFromGenes :: [Genes] -> Loc
def getBoundFromGenes(genes, ext = 5000)
   genesSorted = genes.sort_by{|x| x.tx_start}
   head = genesSorted[0] 
   tail = genesSorted[-1] 
   loc = Loc.new
   loc.chr = head.chr 
   loc.start = head.tx_start.to_i - ext
   loc.end_ = tail.tx_end.to_i + ext
   return loc
end


#### CTCF

# loadCtcf:: FilePath -> (IO) Hash(chr, Ctcf)
def loadCtcf(ctcfFile = "data/ctcf/fimo_ctcf_heart.txt")
   File.read(ctcfFile).split("\n").map{|x| x.split("\t").to_ctcf}.group_by{|x| x.chr} 
end

# interval is query interval, based on the genomic boundary from a list of genes
# selectCtcf :: Loc -> Hash(chr, Ctcf) -> [Ctcf]
def selectCtcf(interval, ctcfHash)
   loc = interval.to_loc 
   ctcfByChr = ctcfHash[loc.chr]

   if ctcfByChr.nil?
   then ctcfByChr = []
   end
   
   ctcfIn = ctcfByChr.select{|ctcf| ctcf.start.to_i >= loc.start && ctcf.end_.to_i <= loc.end_}.sort_by{|x| x.start}
end

# as long as the interval is a String and in Loc format
def select_multi_Ctcf(intervals)
  ctcfHash = loadCtcf()
  intervals.map do
    |interval| selectCtcf(interval, ctcfHash)
  end
end

# merge_gene_ctcf::String -> String -> [Features (Genes/Ctcf)]
def merge_features(interval, mode, indexDir = "mm9")
  peak = interval.to_loc
  ctcfHash = loadCtcf()
  genes = get_symbol_genes(get_expr_genes(findGenes(interval, mode)))
  bound = getBoundFromGenes(genes).to_s
  ctcf = selectCtcf(bound, ctcfHash).select{|ctcf| ctcf.peak_score.to_i >= 30}.sort_by{|x| [x.loc, x.motif_start]}  # use a singular version
  features = genes + ctcf + [peak]

  features_sorted = features.sort_by{|x| x.loc}
  features_sorted.first.label = 0  
  n = features_sorted.size
   for i in 1..(n-1)
    if features_sorted[i].label.nil?
    then features_sorted[i].label = features_sorted[i-1].label
    end
  end

  res = features.sort_by{|x| x.loc}.map{|x| x.to_bed}
end

# split features to upstream of the peak and downstream of the peak
def merge_features_split(interval, mode, indexDir = "mm9")
  peak = interval.to_loc
  ctcfHash = loadCtcf()
  genes = get_symbol_genes(get_expr_genes(findGenes(interval, mode)))
  bound = getBoundFromGenes(genes).to_s

  ctcf = selectCtcf(bound, ctcfHash).select{|ctcf| ctcf.peak_score.to_i >= 30}.sort_by{|x| [x.loc, x.motif_start]}  # use a singular version
  ctcf_labeled = if ctcf.size != 0
                 then label_ctcf(ctcf)
                 else []
                 end

  features = genes + ctcf_labeled + [peak]
  features_sorted = features.sort_by{|x| x.loc}
  features_sorted.first.label = 0  
  n = features_sorted.size
   for i in 1..(n-1)
    if features_sorted[i].label.nil?
    then features_sorted[i].label = features_sorted[i-1].label
    end
  end

  peak_tag = features_sorted.select{|x| x.class == Loc.new.class}.first.label
  genes = features_sorted.select{|x| x.label == peak_tag}.select{|x| x.class == Gene.new.class && !x.expr.nil?}
  promoter = []
  rest_genes = []
  genes.map do
    |g| if (g.loc - peak.loc).abs <= 1000
        then promoter << g
        else rest_genes << g
        end
  end

  result = (promoter + rest_genes.sort_by{|x| x.expr.v10_fpkm.to_f }.reverse).select{|x|  x.expr.v10_fpkm.to_f > 0 }.map{|x| x.to_bed}
end

def label_ctcf(ctcf)
  ctcf.first.label = 0
  n = ctcf.size
  tag = 0
  for i in 1..(n-1)
    if is_convergent?(ctcf[i-1].motif_strand, ctcf[i].motif_strand)
    then ctcf[i].label = tag
    else tag += 1; ctcf[i].label = tag
    end
  end
  ctcf
end

def is_convergent?(ctcf1, ctcf2)
  if ctcf1 == "-" && ctcf2 == "+"
  then false
  else true
  end
end

# 
#### functions work on list of genes

# translate names; ensembl to symbol dictionary is generated by Ensembl2Symbol; genome version for mouse is gencode vM8
# the annotation pair is at ./annotation/unique.vM8.annotation.pairs.txt
# get_symbol_genes :: [Genes] -> [Genes]
def get_symbol_genes(geneList, genome = "mm10", dir = "data/annotation/unique.vM8.annotation.pairs.txt")
  dict = Hash.new
  case genome 
  when "mm10" ## mm9 and mm10 share the same ensembl - symbol pair
    dict = File.read(dir).split("\n").map{|x| x.split(" ")}.map{|x| [x[0].split(".").first, x[1]]}.to_h # space not tab
  end
  geneList.map do
    |x| 
        sym = dict[x.altName]
        if sym.nil?
        then x.symbol = x.ensTxName
        else x.symbol = sym + "_" + x.tx_start.to_s[-3..-1]
        end
        x
  end
end

# get_expr_genes :: [Genes] -> [Genes]
def get_expr_genes(geneList, genome = "mm9", dir = "data/expression/combined_isoform_mm9_heart_compact.txt")
  dict = File.read(dir).split("\n")[1..-1].map{|x| x.split("\t")}.map{|x| [x.first, x.to_expr]}.to_h
  geneList.map do
    |x|
       x.expr = dict[x.ensTxName]
       x
  end
end


def example
   puts findGenes("chr3:128,814,307-128,815,036", "distance,50000,100000").size
   ens = findGenes("chr3:128,814,307-128,815,036", "distance,50000,100000")
   get_expr_genes(get_symbol_genes(ens)).map{|x| x.shorten}
end

# ctcf
def example2
  select_mutli_Ctcf(["chr3:127,814,307-128,815,036"]).first.select{|ctcf| ctcf.peak_score.to_i >= 72}.map{|x| [x.start, x.motif_strand]}
end

# real example
# chr17:26995768-27010158: a cluster of peaks around Nkx2-5
def example3
  genes = findGenes("chr17:26995768-27010158", "distance,50000,50000")
  bound = getBoundFromGenes(genes).to_s
  select_multi_Ctcf([bound]).first.select{|x| x.peak_score.to_i >= 50}.map{|x| [x.start, x.motif_pvalue, x.motif_strand]}
end

def example4
  out = merge_features("chr17:26995768-27010158", "distance,50000,50000").map{|x| x.to_s}.join("\n")
  File.write("test.bed.txt", out)
  nil
end

def example5
  out = merge_features_split("chr4	155589455	155590455", "distance,200000,200000").map{|x| x.to_s}.join("\n")
  File.write("test.bed.txt", out)
  nil
end

def example6
  inputpath = "/Users/minzhang/Documents/data/mz_P9_atac_clustering/data/peak.list.output.short.myh6_over_tcf21.txt"
  input = File.read(inputpath).split("\n")[1..-1].map{|x| x.split("\t") }
  input.map do
    |x| region = [x[1], x[2], x[3]].join(":")
        output = merge_features_split(region, "distance,200000,200000").map{|x| x.name.split("_").first}.uniq.join(";")
        line = x.join("\t") + "\t" + output + "\n"
        File.open("test.peakannotation.txt", 'a'){|f| f.write(line)}
  end
  nil
end

def main
  inputpath = ARGV[0] # narrow peak with header line
  outputpath = ARGV[1]
  input = File.read(inputpath).split("\n")[1..-1].map{|x| x.split("\t") }
  input.map do
    |x| region = [x[1], x[2], x[3]].join(":")
        output = merge_features_split(region, "distance,200000,200000").map{|x| x.name.split("_").first}.uniq.join(";")
        line = x.join("\t") + "\t" + output + "\n"
        File.open(outputpath, 'a'){|f| f.write(line)}
  end
  nil
end

main
