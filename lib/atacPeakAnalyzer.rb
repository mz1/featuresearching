#!/bin/ruby

## Min Zhang
## July 6, 2016

## ATAC-peak analyzer

## looking for exclusive peaks (definitive peaks), shared peaks

## peaks
## super enhancer

load "./atacPeakAnalyzerHeader.rb"

def samples
  neo_cm = Sample.new
  neo_cm.name = "neo_cm"
  neo_cm.path = "/Users/minzhang/Documents/data/P60_heart_dropseq/063016_peakCalling_results/neo_cm/neo_cm_superEnahncer_0_65.homer.ruby.txt"

  neo_endo = Sample.new
  neo_endo.name = "neo_endo"
  neo_endo.path = "/Users/minzhang/Documents/data/P60_heart_dropseq/063016_peakCalling_results/neo_endo/neo_endo_superEnhancers.homer.rubyannotated.txt"  

  adult_tcf_mut = Sample.new
  adult_tcf_mut.name = "ad_tcf_mut"  
  adult_tcf_mut.path = "/Users/minzhang/Documents/data/P60_heart_dropseq/063016_peakCalling_results/adult_cf_tcf21_mut/adult_cf_tcf21_lats_superEnhancers.homer.ruby.txt"

  neo_endo_np = Sample.new
  neo_endo_np.name = "neo_endo_np"
  neo_endo_np.path = "/Users/minzhang/Documents/data/P60_heart_dropseq/063016_peakCalling_results/neo_endo/neo_endo.peaks.txt"

  neo_cm_np = Sample.new
  neo_cm_np.name = "neo_cm_np"
  neo_cm_np.path = "/Users/minzhang/Documents/data/P60_heart_dropseq/063016_peakCalling_results/neo_cm/neo_cm.peaks.homer.txt"

  adult_tcfmut_np = Sample.new
  adult_tcfmut_np.name = "adult_tcfmut_np"
  adult_tcfmut_np.path = "/Users/minzhang/Documents/data/P60_heart_dropseq/063016_peakCalling_results/adult_cf_tcf21_mut/adult_tcfmut_peaks.txt" 

  s = {neo_cm.name => neo_cm, 
       neo_endo.name => neo_endo,
       adult_tcf_mut.name => adult_tcf_mut,
       neo_endo_np.name => neo_endo_np,
       neo_cm_np.name => neo_cm_np,
       adult_tcfmut_np.name => adult_tcfmut_np}

  return s
end

def overlaySuperEnhancers
  s = samples
  neo_cm_se = txt_to_array(s["neo_cm"].path).map{|x| x.to_superEnh}.map{|x| x.source = "neo_cm_se"; x}
  neo_endo_se = txt_to_array(s["neo_endo"].path).map{|x| x.to_superEnh}.map{|x| x.source = "neo_endo_se"; x}
  ad_tcf_mut_se = txt_to_array(s["ad_tcf_mut"].path).map{|x| x.to_superEnh}.map{|x| x.source = "ad_tcf_mut"; x}
 
  cm1 = neo_cm_se
  endo1 = neo_endo_se
  tcfm = ad_tcf_mut_se

 # overlay_se_lists(cm1, endo1).map{|x|x.show}.join("\n")
 # puts overlay_se_lists(cm1, tcfm).map{|x|x.show}.join("\n")
 # puts tcfm.map{|x| x.show}.join("\n")
 # puts tcfm.map{|x| (x.start - x.end).abs}.mean

 # the list of SE is sorted
  all_se = (cm1 + endo1 +tcfm).sort_feature
  puts all_se.size
  merged_se = mergeSE_list(all_se)
  output_bed = merged_se.map{|x| x.to_bed.to_s}.join("\n")
     
  File.open("output_mergedSE.bed", 'w'){|f| f.write(output_bed)}  
  
  # get narrowPeaks for each SuperEnh 
  neo_endo_npHash = loadNarrowPeaks(s["neo_endo_np"].path, "neo_endo_np", 0)
  neo_cm_npHash = loadNarrowPeaks(s["neo_cm_np"].path, "neo_cm_np", 0)
  adult_tcfmut_npHash = loadNarrowPeaks(s["adult_tcfmut_np"].path, "adult_tcfmut", 0)

  npHash_list = [neo_endo_npHash, neo_cm_npHash, adult_tcfmut_npHash]
  res = resort_narrowPeaks(extractNP_list(merged_se, npHash_list))
  # there are duplicated narrowpeaks, don't know why
  out = res.map{|x| x.narrowPeaks}.flatten.uniq.map{|x| x.get_name.to_bed.to_s}.join("\n")
  # header track name="mergedNP" description="mergedNP from three tissues" itemRgb="On" 
  File.open("output_mergedNP.bed", 'w'){|f| f.write(out)} 
  nil
end

## test code
def getNarrowPeaks
  s = samples 
  neo_endo_npHash = loadNarrowPeaks(s["neo_endo_np"].path)
#  output = selectFeature("chr8:59589864-59983168", neo_endo_npHash).map{|x| x.to_loc.to_bed.to_s}.join("\n")
#  File.open("output.bed", 'w'){|f| f.write(output)}
#
  neo_cm_npHash = loadNarrowPeaks(s["neo_cm_np"].path)
  adult_tcfmut_npHash = loadNarrowPeaks(s["adult_tcfmut_np"].path)

  output = selectFeature("chr1:157055196-157094503", neo_endo_npHash).map{|x| x.to_loc.to_bed.to_s}.join("\n")
  File.open("output.bed", 'w'){|f| f.write(output)}

  nil
end


