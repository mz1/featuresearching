
class Feature
  # @return 
  attr_accessor :chr, :start, :end

  class Loc
    attr_accessor :loc, :label

    def to_s
      [self.chr, self.start, self.end].join("_")
    end
   
    def to_bed
      bed = Bed.new
      bed.chr = self.chr
      bed.chrStart = self.start
      bed.chrEnd = self.end
      bed.name = [self.chr, self.start, self.end, self.label.to_s].join("_")
      bed.score = "0"
      bed.strand = "+"
      bed
    end

  end


  def to_interval
    res = Interval.new
    res.start = self.start
    res.end = self.end
    return res
  end

end



class Peak < Feature
  attr_accessor :name, :score
end

class NarrowPeak < Peak
  attr_accessor :motif, :center, :promoter, :source
 
  def to_loc
    loc = Loc.new
    loc.chr = self.chr
    loc.start = self.start
    loc.end = self.end
    loc.label = ""
    loc
  end
  
end

class SuperEnh < Peak
  attr_accessor :narrowPeaks, :annotation, :source

  def initialize
    @narrowPeaks = []
  end

  def show
    annotation = self.annotation == "" ? "" : self.annotation.first
    loc =  self.chr + ":" + self.start.to_s + "-" + self.end.to_s
    [loc, self.source, annotation].join(" ")
  end

  def to_bed
    bed = Bed.new
    bed.chr = self.chr
    bed.chrStart = self.start
    bed.chrEnd = self.end
    bed.name = self.source
    bed.score = "0"
    bed.strand = "+"
    return bed
  end

end

class Bed
  attr_accessor :chr, :chrStart, :chrEnd, :name, :score, :strand
  def to_s
    [self.chr, self.chrStart, self.chrEnd, self.name, self.score, self.strand].join("\t")
  end
 
end

