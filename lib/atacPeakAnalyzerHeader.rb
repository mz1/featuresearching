#!/bin/ruby

## Min Zhang
## July 6, 2016

## ATAC-peak analyzer Header

## looking for exclusive peaks (definitive peaks), shared peaks
## class definition

require 'features.rb'

## from findFeature processed homer annotation file
class Array
  def to_superEnh
    se = SuperEnh.new
    se.chr = self[1]
    se.start = self[2].to_i
    se.end = self[3].to_i
    
    se.score = self[5]
    se.annotation = self[19].nil? ? "" : self[19].split(";")
   
    return se
  end
  
  # integer
  def mean
    len = self.size
    sum = self.inject(:+)
    return sum/len
  end

  # homer peak file with annotation
  def to_np
    np = NarrowPeak.new
    np.chr = self[1]
    np.start = self[2].to_i
    np.end = self[3].to_i
    np.score = self[5].to_i
    np.center = (np.start + np.end) / 2
    np.promoter = if (self[7].start_with?("promoter-TSS") || self[7].start_with?("5' UTR"))
                  then self[15]
                  else nil
                  end
    return np 
  end

  def sort_feature
    self.group_by{|x| x.chr}.map{|x| x[1].sort_by{|x| x.start}}.flatten
  end

end

class String
  def to_loc(sep = ":")
      sArr = self.gsub(",", "").gsub("\t", ":").gsub(" ", ":").gsub("-", ":").split(sep)
      if sArr.size != 3 || sArr[0][0..2] != "chr" || sArr[1] =~/[0-9]/.nil? || sArr[2] =~/[0-9]/.nil?
      then return "Error: bad format for interval"
      else loc = Loc.new
           loc.chr = sArr[0]
           loc.start = sArr[1].to_i
           loc.end = sArr[2].to_i
           loc.loc = loc.start
           loc.label = nil
           return loc
      end
   end

end

class Sample
  attr_accessor :name, :path
end

## can monitor compare pair numbers
## overlay for super enhancers
class Match
  attr_accessor :peakA, :peakB, :isOverlay, :regionOverlay, :npOverlay
  
  def show
    [self.peakA.show, self.peakB.show].join("\t")
  end

end

class Interval
  attr_accessor :start, :end
  
  def out_union(interval2)
    x = [self.start, self.end, interval2.start, interval2.end]
    res_start = x.min
    res_end = x.max

    res = Interval.new
    res.start = res_start
    res.end = res_end
    return res
  end

  def intersect(interval2)
    if [self.start < self.end, 
        interval2.start < interval2.end,
        self.end < interval2.start || self.start > interval2.end].all?
    then return nil
    else 
       res = Interval.new
       x = [self.start, self.end, interval2.start, interval2.end].sort[1..2]
       res.start = x.first
       res.end = x.last
       return res
    end
  end

end
 
def txt_to_array(file, header = false)
  output = File.read(file).split("\n").compact.map{|x| x.split("\t")}
  return (header ? output[1..-1] : output)
end 

#overlay_se::SuperEnh -> SuperEnh -> Match
# change to more general overlay_feature
def overlay_feature(se1, se2)
  res = Match.new
  chr = se1.chr == se2.chr
  interval = se1.to_interval.intersect(se2.to_interval)
  if chr == false 
    then 
      res.isOverlay = false
      return res
  elsif interval.nil?
    then res.isOverlay = false
         return res
  else
      res.isOverlay = true
      res.peakA = se1
      res.peakB = se2
      res.regionOverlay = interval
      return res
  end
end

#overlay_se_lists::[SuperEnh] -> [SuperEnh] -> [Match]
def overlay_feature_lists(seList1, seList2)
  res = seList1.map{|a| seList2.map{|b| overlay_feature(a, b)}}.flatten
  res = res.select{|x| x.isOverlay != false}
  return res
end


# loadNarrowPeaks::file -> npHash(chr, NarrowPeak)
def loadNarrowPeaks(npFile, npsource = "")
  txt_to_array(npFile, header = true).map{|x| x.to_np; x.source = npsource; x}.group_by{|x| x.chr}
end

# selectCtcf ::Feature/Loc/SuperEnh/String(in the format of location) -> Hash(chr, feature) -> [Ctcf]
def selectFeature(interval, featureHash)
   if interval.class == String.new.class
     then loc = interval.to_loc 
   elsif interval.class <= Feature.new.class
     then loc = interval
   end

   featureByChr = featureHash[loc.chr]

   if featureByChr.nil?
   then featureByChr = []
   end
   
   featureIn = featureByChr.select{|f| f.start >= loc.start && f.end <= loc.end}.sort_by{|x| x.start}
  
   return featureIn
end

# merge super enhancers
# mergeSe :: SuperEnhancer -> SuperEnhancer -> SuperEnhancer
# given two se are overlapped
def mergeSE(se1, se2) 
  se_out = SuperEnh.new
  if se1.chr == se2.chr
  then 
     se_out.chr = se1.chr
     interval = se1.to_interval.out_union(se2.to_interval) # union
     se_out.start = interval.start
     se_out.end = interval.end
     se_out.source = se1.source + ";" + se2.source
     se_out.narrowPeaks = []
     return se_out
  else return nil
  end
end

# toy program, recursive for merge duplicate array
def mergeSE_list_toy(seList)
  if seList == []
    then return []
  elsif seList.size == 1
    then return seList
  else
        newHead = [seList[0], seList[1]].uniq
      if newHead.size == 1
      then
        return  mergeSE_list(newHead + seList[2..-1])
      else 
        return [seList[0]] + mergeSE_list(seList[1..-1])
      end
  end
end

## the list of SuperEnh must be sorted by chr and then by start
def mergeSE_list(seList)
  if seList == []
    then return []
  elsif seList.size == 1
    then return seList
  else
      match = overlay_feature(seList[0], seList[1])
      if match.isOverlay
      then
        return mergeSE_list([mergeSE(seList[0], seList[1])] + seList[2..-1])
      else 
        return [seList[0]] + mergeSE_list(seList[1..-1])
      end
  end
end


# similar to mergeSE, but by np center is more important, and instead use out_union interval as for SE, use intersect
def mergeNP(np1, np2, distance = 50)
  np_out = NarrowPeak.new
  if np1.chr == np2.chr && (np1.center - ne2.center).abs <= distance
  then
    np_out.chr = np1.chr
    interval = np1.to_interval.intersect(np2.to_interval) ## intersection
    np_out.start = interval.start
    np_out.end = interval.end
    np_out.source = np1.source + ";" + np2.source 
    return np_out
  else return nil
  end
end

def mergeNP_list(npList)
  if npList == []
    then return []
  elsif npList.size == 1
    then return npList
  else
      match = overlay_feature(npList[0], npList[1])
      if match.isOverlay
      then
        return mergeNP_list([mergeNP(npList[0], npList[1])] + npList[2..-1])
      else 
        return [npList[0]] + mergeNP_list(npList[1..-1])
      end
  end
end

# extractNP_list :: [SuperEnh] -> npHash(chr, NarrowPeak) -> [SuperEnh]
def extractNP_list(se_list, npHash_list)
  
  se_list.map do
    |se| npHash_list.map{|nphash| se.narrowPeaks.push(selectFeature(se, nphash))}
         se.narrowPeaks = se.narrowPeaks.flatten.sort_feature
  end

  se_list 
end


