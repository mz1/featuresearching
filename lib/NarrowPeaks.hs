{-

  Min Zhang
  July 25, 2016

  ATAC-Seq analysis
  
  Fimo parsing and analyze the combination of motifs.

-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module NarrowPeaks
where

import qualified Data.Text as T
import Control.Applicative
import qualified Data.List as L
import qualified Data.Set as Set
import qualified Data.HashMap.Lazy as M
import qualified Data.Maybe as Maybe
import Control.Lens
import Control.Monad (fmap)
import Data.Ord (comparing)
import Data.Function (on)
import qualified Safe as S
import qualified Data.Attoparsec.Text as AP
import qualified Data.Char as C
import qualified Data.Scientific as Sci

import Fimo as F
import Util
import SuperEnhancers as SE


toTupleNp np = (F.name np, np)

-- | make hashmap of narrowpeaks 
mergeNpToHash np = M.fromListWith mergeNarrowPeaks np
  where
    -- | merge two narrow peaks with same names
    mergeNarrowPeaks np1 np2 = 
      NP (name np1) source' (loc np1) tf'
      where source' = Set.toList $ 
                      Set.fromList $
                     (F.source np1 ++ F.source np2)
            tf' =    Set.toList $
                     Set.fromList $
                     (tf np1 ++ tf np2)

-- | combine motifs of NarrowPeaks 
mergeNpToList np = (M.elems . mergeNpToHash) np

-- | Get source of Narrow Peaks
--   what's the combination of different tissues
--   start from HashMap with narrow peak name as key
getSource npMap = M.map F.source npMap
getMotif = M.map (map motif . tf)

filterBySource t = M.filter ((==) t . Set.toList . Set.fromList . F.source)

-- | make tuples for each NP, (matched SE, NP)
findSE seHash np = 
  let seList = lookupChr ((chr . loc) np) seHash
  in matchLoc np seList
  where
    lookupChr chr hash = M.lookupDefault [] chr hash
    
    -- | x:xs is sorted
    matchLoc np [] = (SE.NullSE, np)
    matchLoc np (x:xs) = 
      if isIn np x
      then (x, np)
      else matchLoc np xs 
    
    isIn np se = 
         (start . loc) np >= (start_se se) && 
         (end . loc) np <= (end_se se)

