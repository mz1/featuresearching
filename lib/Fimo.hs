{-

  Min Zhang
  July 25, 2016

  ATAC-Seq analysis
  
  Fimo parsing and analyze the combination of motifs.

-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Fimo
where

import qualified Data.Text as T
import Control.Applicative
import qualified Data.List as L
import qualified Data.Set as Set
import qualified Data.HashMap.Lazy as M
import qualified Data.Maybe as Maybe
import Control.Lens
import Control.Monad (fmap)
import Data.Ord (comparing)
import Data.Function (on)
import qualified Safe as S
import qualified Data.Attoparsec.Text as AP
import qualified Data.Char as C
import qualified Data.Scientific as Sci

import Util

data Tissue = Tissue T.Text
     deriving (Show, Read, Eq, Ord)

data Loc = Loc
           { chr    :: T.Text
           , start  :: Int
           , end    :: Int
           }
           deriving (Show, Read, Eq)
makeLenses ''Loc

data Score = Score
             { score :: Double
             , pval  :: Sci.Scientific
             , qval  :: Sci.Scientific
             }
             deriving (Show, Read, Eq, Ord)
makeLenses ''Score

data Motif = Motif
             { motif  :: T.Text
             , seq    :: T.Text
             , scoreM :: Score
             , start_motif :: Int
             , end_motif   :: Int
             }
             deriving (Show, Read, Eq, Ord)
makeLenses ''Motif

-- | NarrowPeaks contain motif info
data NarrowPeak = NP
           { name   :: T.Text
           , source :: [Tissue]
           , loc    :: Loc
           , tf     :: [Motif]
           } 
           | NullNP 
           deriving (Show, Read)
makeLenses ''NarrowPeak


-- | Parse Fimo results to NarrowPeak 
fimoParser = do
  motifName <- AP.takeTill isTab
  AP.skip isTab
  _ <- AP.takeTill isTab
  AP.skip isTab
  chr <- AP.takeTill isUnderScore
  AP.skip isUnderScore
  locStart <- text2Int <$> AP.takeWhile C.isDigit
  AP.skip isUnderScore
  tissues <- map Tissue . T.splitOn ";" <$> AP.takeTill isTab
  AP.skip isTab
  start_motif <- text2Int <$> AP.takeWhile C.isDigit
  AP.skip isTab
  end_motif <- text2Int <$> AP.takeWhile C.isDigit
  AP.skip isTab
  strand <- AP.takeWhile isStrand
  AP.skip isTab
  motifScore <- AP.double
  AP.skip isTab
  pval <- AP.scientific
  AP.skip isTab
  qval <- AP.scientific
  AP.skip isTab
  seq <- AP.takeTill AP.isEndOfLine
  AP.skip AP.isEndOfLine
  let npName = T.intercalate "_" 
                            [chr
                           , T.pack $ show locStart]
  let source = tissues
  let loc = Loc chr locStart (locStart + 200)
  let score = Score motifScore pval qval
  let tf = [Motif motifName seq score start_motif end_motif]
  return (NP npName source loc tf)
--  return (tissues, start, end, motifScore)

-- | parse multiple lines
parseManyFimo = AP.many1' fimoParser

-- | parse entire file
readFimo txt = AP.maybeResult $ AP.feed (AP.parse parseManyFimo txt) T.empty


