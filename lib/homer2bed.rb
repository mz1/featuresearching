#!/bin/ruby

## Min Zhang
## June 24, 2016

# turn a homer output to a bed file, but instead of homer's pos2bed.pl; keep location information in the name, and score, therefore, we can also generate fasta file with location information for fimo application

# primarily this program is in findFeature, and for dealing with CTCF data

class Bed
  attr_accessor :chr, :chrStart, :chrEnd, :name, :score, :strand
  def to_s
    [self.chr, self.chrStart, self.chrEnd, self.name, self.score, self.strand].join("\t")
  end
end

class Array
    def to_bed
      bed = Bed.new
      bed.chr = self[1]
      bed.chrStart = self[2]
      bed.chrEnd = self[3]
      bed.name = [self[1], self[2], self[3], self[7].split(".").first].join("_") # add score
      bed.score = self[7] # homer findPeak score
      bed.strand = self[4]
      return bed
    end
end

def homer2bed(inputpath, outputpath)
  arr = File.read(inputpath).split("\n").select{|x| x[0] != '#'}.map{|x| x.split("\t")}
  output = arr.map{|x| x.to_bed.to_s }.join("\n")
  File.write(outputpath, output)
end

def main
  homer2bed(ARGV[0], ARGV[1])
end

main

