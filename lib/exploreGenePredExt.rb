#!/bin/ruby

# Min Zhang
# June 23, 2016

# use this script to quickly understand what is the raw data look like (genePredExt formated table from USCS). This particular example is mouse enGene.txt in mm10 genome.

=begin

http://genome.ucsc.edu/goldenPath/gbdDescriptionsOld.html#GenePredictions

Gene Predictions (Extended)

The following definition is used for extended gene prediction tables. In alternative-splicing situations, each transcript has a row in this table.
table genePredExt
"A gene prediction with some additional info."
    (
    ## small int for index
    string name;        	"Name of gene (usually transcript_id from GTF)"
    string chrom;       	"Chromosome name"
    char[1] strand;     	"+ or - for strand"
    uint txStart;       	"Transcription start position"
    uint txEnd;         	"Transcription end position"
    uint cdsStart;      	"Coding region start"
    uint cdsEnd;        	"Coding region end"
    uint exonCount;     	"Number of exons"
    uint[exonCount] exonStarts; "Exon start positions"
    uint[exonCount] exonEnds;   "Exon end positions"
    uint id;            	"Unique identifier"
    string name2;       	"Alternate name (e.g. gene_id from GTF)"
    string cdsStartStat; 	"enum('none','unk','incmpl','cmpl')"
    string cdsEndStat;   	"enum('none','unk','incmpl','cmpl')"
    lstring exonFrames; 	"Exon frame offsets {0,1,2}"
    )

=end

require 'set'

@path = "ensGene.txt"

def main
  input = File.read(@path).split("\n")
##  puts "number of lines: "
##  puts input.size # 94647

##  puts "colomns: "
##  puts "are they all the same length? " # yes, all 16 columns
  
  ## how many unique indices?
  ## 1645
  # index = input.map{|x| x.split("\t").first}.to_set.size
  # they seems ordered, but not really
  
  # are chromosome name ordered? total 45 chromosome names, 
  # index = input.map{|x| x.split("\t")[2]}.to_set.to_a
  # puts index
  # how are transcripts ordered? by TX start site?

  # list of the tx start sites, in the list of chromosomes
  tx = input.map{|x| x.split("\t")}.group_by{|x| x[2]}
  
  # chr13!!!, all the items, tx start sites, set into pairs of neighbors 
  # puts tx.to_a[4][1].map{|x| x[4].to_i }.each_cons(2).map{|x, y| if (x <= y) then 0 else 1 end}.inject(:+)
  # ! wrong: most sorted but not always; actually on chr5, total 2798 items, only 2 not sorted
  # ! wrong: which ones are not sorted?

  # x[4].to_i is important; otherwise it's comparing strings. it is sorted
  puts tx.to_a[4][1].map{|x| x[4].to_i}.each_cons(2).map{|x, y| if (x <= y) then nil else y end}.compact


  
end

module Enumerable
  def sorted?
    each_cons(2).all? { |a, b| a <= b}
  end
end

  
  
