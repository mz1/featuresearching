{-

  Min Zhang
  July 25, 2016

  ATAC-Seq analysis
  
  Fimo parsing and analyze the combination of motifs.

-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

import qualified Data.Text as T
import qualified Data.Text.IO as TextIO
import Control.Applicative
import qualified Data.List as L
import qualified Data.Set as Set
import qualified Data.HashMap.Lazy as M
import qualified Data.Maybe as Maybe
import Control.Lens
import Control.Monad (fmap)
import Data.Ord (comparing)
import Data.Function (on)
import qualified Safe as S
import qualified Data.Attoparsec.Text as AP
import qualified Data.Char as C
import System.IO

import Fimo
import NarrowPeaks
import SuperEnhancers

sample = "/Users/minzhang/Documents/data/P60_heart_dropseq/AtacSeqAnalysis/data/fimo_out/fimo.annotated.txt"

sePath = "/Users/minzhang/Documents/data/P60_heart_dropseq/AtacSeqAnalysis/data/output_mergedSE.bed"

main = do
  fimo <- Maybe.fromJust . readFimo <$> TextIO.readFile sample
  let mergedFimo = mergeNpToList $ map toTupleNp fimo
  se <- Maybe.fromJust . readSE <$> TextIO.readFile sePath
  let seHash = seToHash se
  let res = map (findSE seHash) mergedFimo
  --let res = M.keys seHash
  print $ length mergedFimo
  


